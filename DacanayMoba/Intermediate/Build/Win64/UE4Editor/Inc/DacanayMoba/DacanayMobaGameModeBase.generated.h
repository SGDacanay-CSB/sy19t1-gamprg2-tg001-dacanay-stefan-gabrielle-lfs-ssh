// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef DACANAYMOBA_DacanayMobaGameModeBase_generated_h
#error "DacanayMobaGameModeBase.generated.h already included, missing '#pragma once' in DacanayMobaGameModeBase.h"
#endif
#define DACANAYMOBA_DacanayMobaGameModeBase_generated_h

#define DacanayMoba_Source_DacanayMoba_DacanayMobaGameModeBase_h_15_RPC_WRAPPERS
#define DacanayMoba_Source_DacanayMoba_DacanayMobaGameModeBase_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define DacanayMoba_Source_DacanayMoba_DacanayMobaGameModeBase_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesADacanayMobaGameModeBase(); \
	friend struct Z_Construct_UClass_ADacanayMobaGameModeBase_Statics; \
public: \
	DECLARE_CLASS(ADacanayMobaGameModeBase, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/DacanayMoba"), NO_API) \
	DECLARE_SERIALIZER(ADacanayMobaGameModeBase)


#define DacanayMoba_Source_DacanayMoba_DacanayMobaGameModeBase_h_15_INCLASS \
private: \
	static void StaticRegisterNativesADacanayMobaGameModeBase(); \
	friend struct Z_Construct_UClass_ADacanayMobaGameModeBase_Statics; \
public: \
	DECLARE_CLASS(ADacanayMobaGameModeBase, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/DacanayMoba"), NO_API) \
	DECLARE_SERIALIZER(ADacanayMobaGameModeBase)


#define DacanayMoba_Source_DacanayMoba_DacanayMobaGameModeBase_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ADacanayMobaGameModeBase(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ADacanayMobaGameModeBase) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ADacanayMobaGameModeBase); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ADacanayMobaGameModeBase); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ADacanayMobaGameModeBase(ADacanayMobaGameModeBase&&); \
	NO_API ADacanayMobaGameModeBase(const ADacanayMobaGameModeBase&); \
public:


#define DacanayMoba_Source_DacanayMoba_DacanayMobaGameModeBase_h_15_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ADacanayMobaGameModeBase(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ADacanayMobaGameModeBase(ADacanayMobaGameModeBase&&); \
	NO_API ADacanayMobaGameModeBase(const ADacanayMobaGameModeBase&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ADacanayMobaGameModeBase); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ADacanayMobaGameModeBase); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ADacanayMobaGameModeBase)


#define DacanayMoba_Source_DacanayMoba_DacanayMobaGameModeBase_h_15_PRIVATE_PROPERTY_OFFSET
#define DacanayMoba_Source_DacanayMoba_DacanayMobaGameModeBase_h_12_PROLOG
#define DacanayMoba_Source_DacanayMoba_DacanayMobaGameModeBase_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	DacanayMoba_Source_DacanayMoba_DacanayMobaGameModeBase_h_15_PRIVATE_PROPERTY_OFFSET \
	DacanayMoba_Source_DacanayMoba_DacanayMobaGameModeBase_h_15_RPC_WRAPPERS \
	DacanayMoba_Source_DacanayMoba_DacanayMobaGameModeBase_h_15_INCLASS \
	DacanayMoba_Source_DacanayMoba_DacanayMobaGameModeBase_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define DacanayMoba_Source_DacanayMoba_DacanayMobaGameModeBase_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	DacanayMoba_Source_DacanayMoba_DacanayMobaGameModeBase_h_15_PRIVATE_PROPERTY_OFFSET \
	DacanayMoba_Source_DacanayMoba_DacanayMobaGameModeBase_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	DacanayMoba_Source_DacanayMoba_DacanayMobaGameModeBase_h_15_INCLASS_NO_PURE_DECLS \
	DacanayMoba_Source_DacanayMoba_DacanayMobaGameModeBase_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> DACANAYMOBA_API UClass* StaticClass<class ADacanayMobaGameModeBase>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID DacanayMoba_Source_DacanayMoba_DacanayMobaGameModeBase_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
